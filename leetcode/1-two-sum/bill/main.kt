class Solution {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        for (i:Int in 0..nums.size-2) {
            for (j:Int in i+1..nums.size-1) {
                
                val a = nums[i]   
                val b = nums[j]
                
                if (a + b == target) {
                    return intArrayOf(i, j)
                }
                
            } 
        }
        return intArrayOf()
    }
}
