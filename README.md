Jetbrain Kotlin 社群讀書會延伸的實作組，旨在學習基礎的 Kotlin 知識後，找個實作練習延續 Kotlin 的學習。

我們（大家）會從 [LeetCode](https://leetcode.com/), [CODE FORCES](https://codeforces.com/), 或是 [<<Kotlin程序員面試算法寶典>>](https://www.tenlong.com.tw/products/9787111612124?list_name=srh), [HackerRank](https://www.hackerrank.com/) 這些地方挑選題目後，線上視訊或是討論如何解這些題目。

進行方式採取類似 scrum 的方式進行，每兩週的星期四晚上，大約一個小時，前半段會由主持人帶領大家來探討如何透過 Kotlin 來解前次聚會決定的題目。後半段會進行 planning 決定下次聚會的題目，以及其他討論事項。

每次研習和討論結果會以 git repo 的形式留下。
